# XXX translation of the Debian reference card
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the refcard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: refcard 10.0\n"
"POT-Creation-Date: 2023-12-27 12:56+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING\n"

#. type: Attribute 'lang' of: <article>
#: entries.dbk:4
msgid "en-GB"
msgstr ""

#. type: Content of: <article><title>
#: entries.dbk:6
msgid "Debian Reference Card"
msgstr ""

#. type: Content of: <article><subtitle>
#: entries.dbk:9
msgid "The 101 most important things when using Debian"
msgstr ""

#. type: Content of: <article><articleinfo>
#: entries.dbk:11
msgid ""
"<copyright> <year>2004</year> <year>2010</year> <holder>W. Martin Borgert</"
"holder> </copyright> <copyright> <year>2016</year> <year>2019</year> "
"<year>2023</year> <holder>Holger Wansing</holder> </copyright> <copyright> "
"<year>2023</year> <holder>Your Name (\"Language\")</holder> </copyright>"
msgstr ""

#. type: Content of: <article><articleinfo><legalnotice><para>
#: entries.dbk:27
msgid ""
"This document may be used under the terms of the GNU General Public License "
"version 3 or higher. The license text can be found at <ulink url=\"https://"
"www.gnu.org/copyleft/gpl.html\"><filename>https://www.gnu.org/copyleft/gpl."
"html</filename></ulink> and <filename>/usr/share/common-licenses/GPL-3</"
"filename>."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:34 entries.dbk:376
msgid "APT"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:35
msgid "Debian"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:36
msgid "dpkg"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:37
msgid "reference card"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:38
msgid "basic commands"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:39
msgid "Version"
msgstr ""

#. type: Content of: <article><articleinfo><keywordset><keyword>
#: entries.dbk:40
msgid "Made by"
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:44
msgid "Getting Help"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:47
msgid "man <replaceable>page</replaceable> or man bash"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:49
msgid ""
"Read help page for every command and many configuration files. Also "
"available online: <filename>https://www.manpages.debian.org/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:54
msgid ""
"<replaceable>command</replaceable> &#x00A0; <optional><filename>--</"
"filename>help, <filename>-</filename>h</optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:57
msgid "Brief help for most commands."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:61
msgid ""
"<filename>/usr/share/doc/<optional><replaceable>package-name</replaceable>/</"
"optional></filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:62
msgid ""
"Find all documentation here, optional file <filename>README.Debian</"
"filename> contains specifics."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:67
msgid "<ulink url=\"https://www.debian.org/doc/\">Web documentation</ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:69
msgid ""
"Reference, manuals, FAQs, HOWTOs, etc. at <filename>https://www.debian.org/"
"doc/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:73
msgid ""
"<ulink url=\"https://lists.debian.org/\">Mailing lists</ulink> at "
"<filename>https://lists.debian.org/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:76
msgid ""
"The community is always helpful, search for <filename>users</filename>. Or "
"use other channels like IRC: <filename>https://www.debian.org/support/</"
"filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:81
msgid ""
"<ulink url=\"https://wiki.debian.org/\">The Wiki</ulink> at "
"<filename>https://wiki.debian.org/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:83
msgid "Contains all kind of useful information."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:89
msgid "Installation"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:92
msgid ""
"<ulink url=\"https://www.debian.org/devel/debian-installer/\">Installer</"
"ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:94
msgid ""
"All information about it at <filename>https://www.debian.org/devel/debian-"
"installer/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:98
msgid "<ulink url=\"https://www.debian.org/distrib/\">CD images</ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:101
msgid "Download from <filename>https://www.debian.org/distrib/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:105
msgid "<filename>boot: expert</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:106
msgid ""
"E.g. to set up the network w/o DHCP or to adapt bootloader installation."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:110
msgid ""
"Or use a <ulink url=\"https://www.debian.org/CD/live/\">Live image</ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:112
msgid ""
"Containing the user-friendly Calamares installer: <filename>https://www."
"debian.org/CD/live/</filename>"
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:118
msgid "Bugs"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:121
msgid ""
"<ulink url=\"https://bugs.debian.org/\">Bug Tracking</ulink> at "
"<filename>https://bugs.debian.org/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:124
msgid "All about existing and fixed bugs."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:128
msgid "Package specific"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:129
msgid ""
"See <filename>https://bugs.debian.org/<replaceable>package-name</"
"replaceable>/</filename>, use <filename>wnpp</filename> to ask for new "
"packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:135
msgid "reportbug"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:136
msgid "Report a bug by e-mail."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:139
msgid "<ulink url=\"https://www.debian.org/Bugs/Reporting\">Reporting</ulink>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:141
msgid ""
"Instructions at <filename>https://www.debian.org/Bugs/Reporting</filename>"
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:147
msgid "Configuration"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:150
msgid "<filename>/etc/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:151
msgid ""
"All system configuration files are under directory <filename>/etc/</"
"filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:155
msgid "editor <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:156
msgid ""
"Default text editor.  May be <command>nano</command>, <command>emacs</"
"command>, <command>vi</command>, <command>joe</command>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:162
msgid ""
"<ulink url=\"http://localhost:631\">CUPS</ulink> at <filename>http://"
"hostname:631</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:164
msgid "Browser interface to printing system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:168
msgid "dpkg-reconfigure <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:170
msgid ""
"Reconfigure a package, e.g. <replaceable>keyboard-configuration</"
"replaceable> (keyboard), <replaceable>locales</replaceable> (localization)."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:174
msgid "update-alternatives <replaceable>options</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:176
msgid "Manage command alternatives."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:179
msgid "update-grub"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:180
msgid "After changing <filename>/etc/default/grub</filename>."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:186
msgid "Daemons and System"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:189
msgid ""
"<filename>systemctl restart <replaceable>name</replaceable>.service</"
"filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:191
msgid "Restart a service, system daemon."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:195
msgid ""
"<filename>systemctl stop <replaceable>name</replaceable>.service</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:197
msgid "Stop a service, system daemon."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:201
msgid ""
"<filename>systemctl start <replaceable>name</replaceable>.service</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:203
msgid "Start a service, system daemon."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:207
msgid "systemctl halt"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:208
msgid "Halts system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:211
msgid "systemctl reboot"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:212
msgid "Reboots system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:215
msgid "systemctl poweroff"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:216
msgid "Shuts down system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:219
msgid "systemctl suspend"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:220
msgid "Suspends system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:223
msgid "systemctl hibernate"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:224
msgid "Hibernates system."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:227
msgid "<filename>/var/log/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:228
msgid "All log files are under this directory."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:232
msgid "<filename>/etc/default/</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:233
msgid "Default values for many daemons and services."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:239
msgid "Important Shell Commands"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:242
msgid "cat <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:243
msgid "Print files to screen."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:246
msgid "cd <replaceable>directory</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:247
msgid "Change to directory."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:250
msgid "cp <replaceable>files</replaceable> <replaceable>dest</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:252
msgid "Copy files and directories."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:255
msgid "echo <replaceable>string</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:256
msgid "Echo string to screen."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:259
msgid ""
"gzip, bzip2, xz <optional><filename>-d</filename></optional> "
"<replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:262
msgid "Compress, uncompress files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:265
msgid "pager <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:266
msgid "Show contents of files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:269
msgid "ls <optional><replaceable>files</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:271
msgid "List files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:274
msgid "mkdir <replaceable>directory-names</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:276
msgid "Create directories."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:279
msgid "mv <replaceable>file1</replaceable> <replaceable>file2</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:281
msgid "Move, rename files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:284
msgid "rm <replaceable>files</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:285
msgid "Remove files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:288
msgid "rmdir <replaceable>dirs</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:289
msgid "Remove empty directories."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:292
msgid ""
"tar <optional>c</optional><optional>x</optional><optional>t</"
"optional><optional>z</optional><optional>j</optional><optional>J</optional> -"
"f <replaceable>file</replaceable>.tar <optional><replaceable>files</"
"replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:296
msgid ""
"Create (c), extract (x), list table of (t)  archive file, <emphasis>z</"
"emphasis> for <filename>.gz</filename>, <emphasis>j</emphasis> for "
"<filename>.bz2</filename>, <emphasis>J</emphasis> for <filename>.xz</"
"filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:303
msgid "find <replaceable>directories expressions</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:305
msgid ""
"Find files like <literal>-name <replaceable>name</replaceable></literal> or "
"<literal>-size <replaceable>+1000</replaceable></literal>, etc."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:311
msgid ""
"grep <replaceable>search-string</replaceable> <replaceable>files</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:313
msgid "Find search-string in files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:316
msgid "ln -s <replaceable>file</replaceable> <replaceable>link</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:318
msgid "Create a symbolic link to a file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:322
msgid "ps <optional><replaceable>options</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:324
msgid "Show current processes."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:327
msgid ""
"kill <optional><replaceable>-9</replaceable></optional> <replaceable>PID</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:330
msgid ""
"Send signal to process (e.g. terminate it). Use <command>ps</command> for "
"PID."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:334
msgid "su - <optional><replaceable>username</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:336
msgid "Become another user, e.g. <filename>root</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:340
msgid "sudo <replaceable>command</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:341
msgid ""
"Execute a command as <filename>root</filename> as normal user, see "
"<filename>/etc/sudoers</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:346
msgid ""
"<replaceable>command</replaceable> <filename>&gt;</filename> "
"<replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:349
msgid "Overwrite file with output of command."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:353
msgid ""
"<replaceable>command</replaceable> <filename>&gt;&gt;</filename> "
"<replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:356
msgid "Append output of command to file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:360
msgid ""
"<replaceable>cmd1</replaceable> <filename>|</filename> <replaceable>cmd2</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:363
msgid "Use output of command 1 as input of command 2."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:367
msgid ""
"<replaceable>command</replaceable> <filename>&lt;</filename> "
"<replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:370
msgid "Use file as input for command."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:379
msgid "apt update"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:380
msgid ""
"Update packages listings from package repositories as listed in <filename>/"
"etc/apt/sources.list</filename>. Required whenever that file or the contents "
"of the repositories change."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:387
msgid "apt search <replaceable>search-string</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:389
msgid ""
"Search packages and descriptions for <replaceable>search-string</"
"replaceable>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:393
msgid "apt list -a <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:395
msgid "Show versions and archive areas of available packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:399
msgid "apt show -a <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:401
msgid "Show package information incl. description."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:405
msgid "apt install <replaceable>package-names</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:407
msgid "Install packages from repositories with all dependencies."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:411
msgid "apt upgrade"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:412
msgid "Install newest versions of all packages currently installed."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:416
msgid "apt full-upgrade"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:417
msgid ""
"Like <command>apt upgrade</command>, but with advanced conflict resolution."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:421
msgid "apt remove <replaceable>package-names</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:423
msgid "Remove packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:426
msgid "apt autoremove"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:427
msgid "Remove packages that no other packages depend on."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:431
msgid "apt depends <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:433
msgid "List all packages needed by the one given."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:437
msgid "apt rdepends <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:439
msgid "List all packages that need the one given."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:443
msgid "apt-file update"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:444
msgid ""
"Update content listings from package repositories, see <command>apt update</"
"command>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:449
msgid "apt-file search <replaceable>file-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:451
msgid "Search packages for file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:454
msgid "apt-file list <replaceable>package-name</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:456
msgid "List contents of a package."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:459
msgid "aptitude"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:460
msgid "Console interface to APT, needs <filename>aptitude</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:464
msgid "synaptic"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:465
msgid "GUI interface to APT, needs <filename>synaptic</filename>."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:471
msgid "Dpkg"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:474
msgid "dpkg -l <optional><replaceable>names</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:476
msgid "List packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:479
msgid "dpkg -I <replaceable>pkg</replaceable>.deb"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:481
msgid "Show package information."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:484
msgid "dpkg -c <replaceable>pkg</replaceable>.deb"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:486
msgid "List contents of package file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:490
msgid "dpkg -S <replaceable>filename</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:492
msgid "Show which package a file belongs to."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:496
msgid "dpkg -i <replaceable>pkg</replaceable>.deb"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:498
msgid "Install package files."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:501
msgid "dpkg -V <optional><replaceable>package-names</replaceable></optional>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:503
msgid "Audit check sums of installed packages."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:506
msgid ""
"dpkg-divert <optional>options</optional> <replaceable>file</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:508
msgid "Override a package&apos;s version of a file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:512
msgid ""
"dpkg <filename>--compare- versions </filename> <replaceable>v1</replaceable> "
"gt <replaceable>v2</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:515
msgid "Compare version numbers; view results with <command>echo $?</command>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:519
msgid ""
"dpkg-query -W &#x00A0;&#x00A0;&#x00A0;&#x00A0; <filename>--showformat</"
"filename>=<replaceable> format</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para><screen>
#: entries.dbk:522
#, no-wrap
msgid "'${Package}"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para><screen>
#: entries.dbk:522
#, no-wrap
msgid "${Version}"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para><screen>
#: entries.dbk:523
#, no-wrap
msgid "${Installed-Size}\\n'"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:521
msgid ""
"Query installed packages, format e.g. <placeholder type=\"screen\" id=\"0\"/"
"> <placeholder type=\"screen\" id=\"1\"/> <placeholder type=\"screen\" "
"id=\"2\"/>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:526
msgid ""
"dpkg <filename>--get-selections</filename> &gt; <replaceable>file</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:528
msgid "Write package selections to file."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:532
msgid ""
"dpkg <filename>--set-selections</filename> &lt; <replaceable>file</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:534
msgid "Set package selections from file."
msgstr ""

#. type: Content of: <article><section><title>
#: entries.dbk:540
msgid "The Network"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:543
msgid "<filename>/etc/network/interfaces</filename>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:544
msgid "Interface configuration (if not controlled via network-manager)."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:548
msgid ""
"if <optional>up</optional><optional>down</optional> <replaceable>device</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:550
msgid "Start, stop network interfaces according to the file above."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:554
msgid "ip"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:555
msgid ""
"Show and manipulate network interfaces and routing, needs "
"<filename>iproute2</filename>."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:559
msgid "ssh -X <replaceable>user@host</replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:561
msgid "Login at another machine."
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossterm>
#: entries.dbk:564
msgid ""
"scp <replaceable>files</replaceable> <replaceable>user@host:path</"
"replaceable>"
msgstr ""

#. type: Content of: <article><section><glosslist><glossentry><glossdef><para>
#: entries.dbk:566
msgid "Copy files to other machine (and vice versa)."
msgstr ""
